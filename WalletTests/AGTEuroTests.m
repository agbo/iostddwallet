//
//  AGTEuroTests.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 07/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AGTEuro.h"
#import "AGTMoney.h"

@interface AGTEuroTests : XCTestCase

@end

@implementation AGTEuroTests

-(void)testMultiplication{
    
    AGTEuro *euro = [AGTMoney euroWithAmount:5];
    AGTEuro *ten = [AGTMoney euroWithAmount:10];
    AGTEuro *total = [euro times:2];
    
    
    XCTAssertEqualObjects(total, ten, @"€5 * 2 should be €10");
    
}

-(void) testEquality{
    
    AGTEuro *five = [AGTMoney euroWithAmount:5];
    AGTEuro *ten = [AGTMoney euroWithAmount:10];
    AGTEuro *total = [five times:2];
    
    XCTAssertEqualObjects(ten, total, @"Equivalent objects should be equal!");
    
    
}

-(void) testHash{
    
    AGTEuro *a = [AGTMoney euroWithAmount:2];
    AGTEuro *b = [AGTMoney euroWithAmount:2];
    
    XCTAssertEqual([a hash], [b hash], @"Equal objects must have same hash");
}

-(void) testAmountStorage{
    
    AGTEuro *euro = [AGTMoney euroWithAmount:2];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    XCTAssertEqual(2, [[euro performSelector:@selector(amount)]integerValue], @"The value retrieved should be the same as the stored");
#pragma clang diagnostic pop

    
}


























@end
