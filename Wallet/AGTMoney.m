//
//  AGTMoney.m
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 07/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTMoney.h"
#import "NSObject+GNUStepAddons.h"


@interface AGTMoney ()
@property (nonatomic, strong) NSNumber *amount;
@end
@implementation AGTMoney

+(id) euroWithAmount:(NSInteger) amount{
    return [[AGTMoney alloc] initWithAmount:amount currency:@"EUR"];
}

+(id) dollarWithAmount:(NSInteger) amount{
    return [[AGTMoney alloc] initWithAmount:amount currency:@"USD"];
}



-(id)initWithAmount:(NSInteger) amount currency:(NSString *)currency{
    if (self = [super init]) {
        _amount = @(amount);
        _currency = currency;
    }
    return self;
}

-(id) times:(NSInteger) multiplier{
   
    AGTMoney *newMoney = [[AGTMoney alloc]
                            initWithAmount:[self.amount integerValue] * multiplier currency:self.currency] ;
    
    return newMoney;
 
}

-(AGTMoney *) plus:(AGTMoney *) other{
    
    NSInteger totalAmount = [self.amount integerValue] +
    [other.amount integerValue];
    
    AGTMoney *total = [[AGTMoney alloc] initWithAmount:totalAmount
                                              currency:self.currency];
    
    return total;
    
}




#pragma mark - Overwritten
-(NSString *) description{
    
    return [NSString stringWithFormat:@"<%@ %ld>",
            [self class], (long)[self amount]];
    
}

-(BOOL)isEqual:(id)object{
    
    if ([self.currency isEqual:[object currency]]) {
        return [self amount] == [object amount];
    }else{
        return NO;
    }
    
}

-(NSUInteger) hash{
    
    return (NSUInteger) self.amount;
}












@end
