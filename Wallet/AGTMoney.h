//
//  AGTMoney.h
//  Wallet
//
//  Created by Fernando Rodríguez Romero on 07/07/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGTMoney : NSObject

@property (nonatomic, readonly) NSString *currency;

+(id) euroWithAmount:(NSInteger) amount;
+(id) dollarWithAmount:(NSInteger) amount;

-(id)initWithAmount:(NSInteger) amount
           currency: (NSString *) currency;

-(id) times:(NSInteger) multiplier;

-(AGTMoney *) plus:(AGTMoney *) other;

@end














